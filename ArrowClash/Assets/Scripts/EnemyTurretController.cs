﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class EnemyTurretController : MonoBehaviour
{
    [SerializeField] private List<StaticEnemyTurret> staticEnemyTurrets;
    [SerializeField] private Vector3 rotateVector = new Vector3(0, .5f, 0f);

    public List<StaticEnemyTurret> StaticEnemyTurrets => staticEnemyTurrets;

    private void Awake()
    {
        staticEnemyTurrets = new List<StaticEnemyTurret>();
        staticEnemyTurrets.AddRange(FindObjectsOfType<StaticEnemyTurret>());

        staticEnemyTurrets.ForEach(x =>
        {
            Observable.Interval(TimeSpan.FromSeconds(x.ShotInterval))
            .Do(_ => x.Shoot())
            .Subscribe();
        }
        );
    }

    private void FixedUpdate()
    {
        staticEnemyTurrets.ForEach(x => x.transform.Rotate(rotateVector, Space.Self));
    }
}
