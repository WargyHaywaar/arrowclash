﻿using UnityEngine;

public class ZenithWeapon : MonoBehaviour, IWeapon
{
    private IShotManager shotManager;
    public void Initialize(IShotManager shotManager)
    {
        this.shotManager = shotManager;
    }

    public void Shoot()
    {
        var go = shotManager.GetInstanceFromPool();
        go.SetActive(true);
        go.transform.position = transform.position;
        var rigidbody = go.GetComponent<Rigidbody>();
        rigidbody.velocity = Vector3.zero;
        rigidbody.AddForce(transform.forward * shotManager.ShotForce);
    }
}
