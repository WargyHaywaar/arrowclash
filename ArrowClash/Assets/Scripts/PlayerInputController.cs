﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Player))]
    public class PlayerInputController : MonoBehaviour
    {
        private Player player;

        private void Awake()
        {
            player = GetComponent<Player>();
            if (player == null)
                Debug.LogError("Missing player component!");
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                Debug.Log($"player {player.UserId} shoots!");
                player.Shoot();
            }

            player.Aim();
        }
    }
}