﻿using UnityEngine;
using System.Linq;

namespace Assets.Scripts
{
    public class Player : MonoBehaviour, IPlayer
    {
        [SerializeField] private string userId;
        [SerializeField] private int gold;
        [SerializeField] private int health;
        [SerializeField] private ShotManager playerShotManager;
        [SerializeField] private GameObject pointer;

        private IWeapon weapon;

        //TODO - удоли
        [SerializeField] private EnemyTurretController enemyTurretController;

        public string UserId => userId;

        public int Gold => gold;

        public int Health => health;

        public IWeapon Weapon => weapon;

        private void Awake()
        {
            weapon = GetComponentInChildren<IWeapon>();
            weapon.Initialize(playerShotManager);

            if (weapon == null)
                Debug.LogError("No weapon found");

            enemyTurretController = FindObjectOfType<EnemyTurretController>();
            if (enemyTurretController == null)
                Debug.LogError("No enemy turret controller found!");

        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Shot")
            {
                health--;
            }
        }

        public void Shoot()
        {
            weapon.Shoot();
        }

        public void Aim()
        {
            var turret = enemyTurretController.StaticEnemyTurrets.OrderBy(x => Vector3.Distance(transform.position, x.transform.position)).FirstOrDefault();

            pointer.transform.LookAt(turret.transform);
        }
    }
}