﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    [SerializeField]
    private float thresholdMaxDistance = 20.0f;

    [SerializeField]
    private float thresholdMinDistance = 10.0f;

    [SerializeField]
    private float cameraSpeed = 10.0f;

    [SerializeField]
    private float camRotationSpeed = 10.0f;

    [SerializeField]
    private float actualDistance;
    private bool coroutineStarted = false;


    void FixedUpdate()
    {
        Vector3 targetDir = target.position - transform.position;
        float step = camRotationSpeed * Time.deltaTime;
        float moveStep = cameraSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
        actualDistance = Vector3.Distance(transform.position, target.position);

        float distanceDelta = actualDistance - thresholdMaxDistance;

        if (distanceDelta > 0)
        {
            Vector3 currentPosition = Vector3.Lerp(transform.position, target.position, cameraSpeed * distanceDelta * Time.fixedDeltaTime);
            transform.position = currentPosition;
        }
        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);


    }

    private IEnumerator moveToTarget(Vector3 targetDir)
    {
        Debug.Log("Coroutine Started");
        int steps = 30;
        coroutineStarted = true;
        for (int i = 0; i < steps; i++)
        {
            float step = camRotationSpeed * Time.deltaTime;

            // The step size is equal to speed times frame time.
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);

            // Move our position a step closer to the target.
            transform.rotation = Quaternion.LookRotation(newDir);
            yield return new WaitForSeconds(.03f);
        }
        Debug.Log("Coroutine Finished");

        coroutineStarted = false;
    }
}
