﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ZenithShot : MonoBehaviour, IShot
{
    [SerializeField] private float ttl;
    public float Ttl
    {
        get => ttl;
        set { ttl = value; }
    }
}
