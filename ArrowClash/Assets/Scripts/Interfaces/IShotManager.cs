﻿using UnityEngine;

public interface IShotManager
{
    float ShotForce { get; }
    GameObject GetInstanceFromPool();
}
