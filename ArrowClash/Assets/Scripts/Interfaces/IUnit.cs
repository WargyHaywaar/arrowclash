﻿using UnityEngine;

public interface IUnit
{
    int Health { get; }
}
