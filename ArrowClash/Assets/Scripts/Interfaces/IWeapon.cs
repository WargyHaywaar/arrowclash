﻿public interface IWeapon
{
    void Initialize(IShotManager shotManager);
    void Shoot();
}
