﻿public interface IPlayer : IUnit
{
    string UserId { get; }
    int Gold { get; }
    IWeapon Weapon { get; } 
}
