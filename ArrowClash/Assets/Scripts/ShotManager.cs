﻿using System.Collections.Generic;
using UnityEngine;

public class ShotManager : MonoBehaviour, IShotManager
{
    [SerializeField]
    private ZenithShot zenithPrefab;
    [SerializeField]
    private int poolSize = 50;
    //Пул увеличивается на 30% если количество снарядов в нём меньшше данной величины
    [SerializeField]
    private int poolResizeLimit = 3;
    [SerializeField]
    private float startZenithForce = 10f;

    private List<IShot> activeShots = new List<IShot>();

    private List<IShot> inactiveShots = new List<IShot>();

    private GameObject shotPool;

    public float ShotForce => startZenithForce;

    public void Awake()
    {
        shotPool = new GameObject("ShotPool");
        GenerateShots(poolSize);
    }

    void LateUpdate()
    {
        foreach (ZenithShot shot in activeShots.ToArray())
        {
            if (shot.Ttl <= 0)
            {
                MoveShotToPool(shot);
            }
            else
                shot.Ttl -= Time.deltaTime;
        }
    }

    public void MoveShotToPool(ZenithShot shot)
    {
        shot.gameObject.SetActive(false);
        activeShots.Remove(shot);
        inactiveShots.Add(shot);
    }

    public GameObject GetInstanceFromPool()
    {
        if (inactiveShots.Count <= poolResizeLimit)
        {
            GenerateShots(poolSize);
        }


        var shot = (ZenithShot)inactiveShots[0];
        shot.Ttl = zenithPrefab.Ttl;
        inactiveShots.Remove(shot);
        activeShots.Add(shot);

        return shot.gameObject;
    }

    private void GenerateShots(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject go = GameObject.Instantiate(zenithPrefab.gameObject, Vector3.zero, Quaternion.identity, shotPool.transform);
            go.SetActive(false);
            ZenithShot zs = go.GetComponent<ZenithShot>();
            inactiveShots.Add(zs);
        }
    }
}
