﻿using System.Collections.Generic;
using UnityEngine;

public class StaticEnemyTurret : MonoBehaviour, IUnit
{
    //TODO - перенести в конструктор
    [SerializeField] private ShotManager shotManager;
    [SerializeField] private List<ZenithWeapon> weapons;
    [SerializeField] private int health;
    [SerializeField] private bool rotatable;
    [SerializeField] private float shotInterval = 1.0f;

    public int Health => health;
    public bool Rotatable => rotatable;
    public float ShotInterval => shotInterval;

    public void Shoot()
    {
        weapons.ForEach(x => x.Shoot());
    }
    private void Awake()
    {
        Initialize();
    }
    private void Initialize()
    {
        weapons.ForEach(x => x.Initialize(shotManager));
    }
}
